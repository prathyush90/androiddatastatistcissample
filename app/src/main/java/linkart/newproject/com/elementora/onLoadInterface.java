package linkart.newproject.com.elementora;

import java.util.List;

import linkart.newproject.com.elementora.Models.AppHistory;
import linkart.newproject.com.elementora.Models.CallHistory;
import linkart.newproject.com.elementora.Models.SmsHistory;

/**
 * Created by prathyush on 11/07/16.
 */
public interface onLoadInterface {

    public void onCallHistory(List<CallHistory>callHistoryList);
    public void onSmsHistory(List<SmsHistory>smsHistoryList);
    public void onAppHistory(List<AppHistory>appHistoryList);
    public void onCallHourWise(List<CallHistory>callHistoryList);
}
