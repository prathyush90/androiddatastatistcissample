package linkart.newproject.com.elementora.LoaderManagerCallbacks;

import android.content.Context;


import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import java.util.Calendar;

import linkart.newproject.com.elementora.DataSources.SmsHistoryDataSource;
import linkart.newproject.com.elementora.DataSources.WebHistoryDataSource;
import linkart.newproject.com.elementora.DbHelper;
import linkart.newproject.com.elementora.Models.SmsHistory;
import linkart.newproject.com.elementora.Models.WebHistory;
import linkart.newproject.com.elementora.UIActivities.MainActivity;
import linkart.newproject.com.elementora.onInitialLoad;

/**
 * Created by prathyush on 11/07/16.
 */
public class OutgoingSms implements LoaderManager.LoaderCallbacks<Cursor> {
    private Context mContext;
    private int count = 0;
    private onInitialLoad mLoad;


    public OutgoingSms(Context applicationContext,onInitialLoad mLoad) {

        this.mContext = applicationContext;
        this.mLoad    = mLoad;
        SharedPreferences mPreference = PreferenceManager.getDefaultSharedPreferences(mContext);
        count    = mPreference.getInt("smsCount",0);

    }


    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        return new CursorLoader(mContext, Uri.parse("content://sms/"),
                null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        switch (loader.getId())
        {
            case MainActivity.SMS_LOADER :
               new backGroundTask().execute(data);
        }

    }



    @Override
    public void onLoaderReset(Loader loader) {

    }

    public class backGroundTask extends AsyncTask<Cursor,Void,Void>
    {

        @Override
        protected Void doInBackground(Cursor... params) {
            DbHelper helper = new DbHelper(mContext);
            SQLiteDatabase database = helper.getWritableDatabase();
            Cursor data = params[0];
            if(data.getCount() != count)
            {
                SmsHistoryDataSource mDataSource = new SmsHistoryDataSource(database);

                while (data.moveToNext() && !data.isClosed())
                {
                    SmsHistory callHistory = new SmsHistory();
                    Calendar calendar    = Calendar.getInstance();
                    calendar.setTimeInMillis(data.getLong(data.getColumnIndex("date")));
                    callHistory.setYear(calendar.get(Calendar.YEAR));
                    callHistory.setMonth(calendar.get(Calendar.MONTH));
                    callHistory.setDay(calendar.get(Calendar.DAY_OF_MONTH));
                    callHistory.setHour(calendar.get(Calendar.HOUR));
                    callHistory.setMin(calendar.get(Calendar.MINUTE));
                    callHistory.setSec(calendar.get(Calendar.SECOND));
                    callHistory.setFormattednumber(data.getString(data.getColumnIndex("address")));
                    callHistory.setType(data.getInt(data.getColumnIndex("type")));
                    callHistory.setRead(data.getString(data.getColumnIndex("read")));
                    callHistory.setMilliseconds(calendar.getTimeInMillis());



                    mDataSource.insert(callHistory);
                }

                count = data.getCount();
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
                editor.putInt("smsCount",count);
                editor.commit();


            }

            if(mLoad != null)
            {
                mLoad.onSmsHistory();
            }
            database.close();
            return null;
        }
    }
}
