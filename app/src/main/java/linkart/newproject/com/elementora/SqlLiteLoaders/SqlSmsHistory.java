package linkart.newproject.com.elementora.SqlLiteLoaders;

import android.content.Context;

import java.util.List;

import linkart.newproject.com.elementora.DataSources.DataSource;
import linkart.newproject.com.elementora.Models.CallHistory;
import linkart.newproject.com.elementora.Models.SmsHistory;
import linkart.newproject.com.elementora.SqlLiteReaderHelper.AbstractDataLoader;
import linkart.newproject.com.elementora.SqlLiteReaderHelper.ContentChangingTask;

/**
 * Created by prathyush on 11/07/16.
 */
public class SqlSmsHistory extends AbstractDataLoader<List<SmsHistory>> {

    private DataSource<SmsHistory> mDataSource;
    private String mSelection;
    private String[] mSelectionArgs;
    private String mGroupBy;
    private String mHaving;
    private String mOrderBy;

    public SqlSmsHistory(Context context, DataSource dataSource, String selection, String[] selectionArgs,
                         String groupBy, String having, String orderBy) {
        super(context);
        mDataSource = dataSource;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mGroupBy = groupBy;
        mHaving = having;
        mOrderBy = orderBy;
    }

    @Override
    protected List<SmsHistory> buildList() {
        List<SmsHistory> testList = mDataSource.read(mSelection, mSelectionArgs, mGroupBy, mHaving,
                mOrderBy);
        return testList;
    }

    public void insert(SmsHistory entity) {
        new InsertTask(this).execute(entity);
    }

    public void update(SmsHistory entity) {
        new UpdateTask(this).execute(entity);
    }

    public void delete(SmsHistory entity) {
        new DeleteTask(this).execute(entity);
    }

    private class InsertTask extends ContentChangingTask<SmsHistory, Void, Void> {
        InsertTask(SqlSmsHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(SmsHistory... params) {
            mDataSource.insert(params[0]);
            return (null);
        }
    }

    private class UpdateTask extends ContentChangingTask<SmsHistory, Void, Void> {
        UpdateTask(SqlSmsHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(SmsHistory... params) {
            mDataSource.update(params[0]);
            return (null);
        }
    }

    private class DeleteTask extends ContentChangingTask<SmsHistory, Void, Void> {
        DeleteTask(SqlSmsHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(SmsHistory... params) {
            mDataSource.delete(params[0]);
            return (null);
        }
    }
}
