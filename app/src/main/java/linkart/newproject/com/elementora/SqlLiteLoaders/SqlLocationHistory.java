package linkart.newproject.com.elementora.SqlLiteLoaders;

import android.content.Context;

import java.util.List;

import linkart.newproject.com.elementora.DataSources.DataSource;
import linkart.newproject.com.elementora.Models.CallHistory;
import linkart.newproject.com.elementora.Models.LocationDetails;
import linkart.newproject.com.elementora.SqlLiteReaderHelper.AbstractDataLoader;
import linkart.newproject.com.elementora.SqlLiteReaderHelper.ContentChangingTask;

/**
 * Created by prathyush on 11/07/16.
 */
public class SqlLocationHistory extends AbstractDataLoader<List<LocationDetails>> {

    private DataSource<LocationDetails> mDataSource;
    private String mSelection;
    private String[] mSelectionArgs;
    private String mGroupBy;
    private String mHaving;
    private String mOrderBy;

    public SqlLocationHistory(Context context, DataSource dataSource, String selection, String[] selectionArgs,
                              String groupBy, String having, String orderBy) {
        super(context);
        mDataSource = dataSource;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mGroupBy = groupBy;
        mHaving = having;
        mOrderBy = orderBy;
    }

    @Override
    protected List<LocationDetails> buildList() {
        List<LocationDetails> testList = mDataSource.read(mSelection, mSelectionArgs, mGroupBy, mHaving,
                mOrderBy);
        return testList;
    }

    public void insert(LocationDetails entity) {
        new InsertTask(this).execute(entity);
    }

    public void update(LocationDetails entity) {
        new UpdateTask(this).execute(entity);
    }

    public void delete(LocationDetails entity) {
        new DeleteTask(this).execute(entity);
    }

    private class InsertTask extends ContentChangingTask<LocationDetails, Void, Void> {
        InsertTask(SqlLocationHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(LocationDetails... params) {
            mDataSource.insert(params[0]);
            return (null);
        }
    }

    private class UpdateTask extends ContentChangingTask<LocationDetails, Void, Void> {
        UpdateTask(SqlLocationHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(LocationDetails... params) {
            mDataSource.update(params[0]);
            return (null);
        }
    }

    private class DeleteTask extends ContentChangingTask<LocationDetails, Void, Void> {
        DeleteTask(SqlLocationHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(LocationDetails... params) {
            mDataSource.delete(params[0]);
            return (null);
        }
    }
}
