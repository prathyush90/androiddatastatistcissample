package linkart.newproject.com.elementora.SqlLiteLoaders;

import android.content.Context;

import java.util.List;

import linkart.newproject.com.elementora.DataSources.DataSource;
import linkart.newproject.com.elementora.Models.AppHistory;
import linkart.newproject.com.elementora.Models.CallHistory;
import linkart.newproject.com.elementora.SqlLiteReaderHelper.AbstractDataLoader;
import linkart.newproject.com.elementora.SqlLiteReaderHelper.ContentChangingTask;

/**
 * Created by prathyush on 11/07/16.
 */
public class SqlCallHistory extends AbstractDataLoader<List<CallHistory>> {

    private DataSource<CallHistory> mDataSource;
    private String mSelection;
    private String[] mSelectionArgs;
    private String mGroupBy;
    private String mHaving;
    private String mOrderBy;

    public SqlCallHistory(Context context, DataSource dataSource, String selection, String[] selectionArgs,
                          String groupBy, String having, String orderBy) {
        super(context);
        mDataSource = dataSource;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mGroupBy = groupBy;
        mHaving = having;
        mOrderBy = orderBy;
    }

    @Override
    protected List<CallHistory> buildList() {
        List<CallHistory> testList = mDataSource.read(mSelection, mSelectionArgs, mGroupBy, mHaving,
                mOrderBy);
        return testList;
    }

    public void insert(CallHistory entity) {
        new InsertTask(this).execute(entity);
    }

    public void update(CallHistory entity) {
        new UpdateTask(this).execute(entity);
    }

    public void delete(CallHistory entity) {
        new DeleteTask(this).execute(entity);
    }

    private class InsertTask extends ContentChangingTask<CallHistory, Void, Void> {
        InsertTask(SqlCallHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(CallHistory... params) {
            mDataSource.insert(params[0]);
            return (null);
        }
    }

    private class UpdateTask extends ContentChangingTask<CallHistory, Void, Void> {
        UpdateTask(SqlCallHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(CallHistory... params) {
            mDataSource.update(params[0]);
            return (null);
        }
    }

    private class DeleteTask extends ContentChangingTask<CallHistory, Void, Void> {
        DeleteTask(SqlCallHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(CallHistory... params) {
            mDataSource.delete(params[0]);
            return (null);
        }
    }
}
