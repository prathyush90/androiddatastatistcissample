package linkart.newproject.com.elementora.SqlLiteLoaders;

import android.content.Context;

import java.util.List;

import linkart.newproject.com.elementora.DataSources.DataSource;
import linkart.newproject.com.elementora.Models.SmsHistory;
import linkart.newproject.com.elementora.Models.WebHistory;
import linkart.newproject.com.elementora.SqlLiteReaderHelper.AbstractDataLoader;
import linkart.newproject.com.elementora.SqlLiteReaderHelper.ContentChangingTask;

/**
 * Created by prathyush on 11/07/16.
 */
public class SqlWebHistory extends AbstractDataLoader<List<WebHistory>> {

    private DataSource<WebHistory> mDataSource;
    private String mSelection;
    private String[] mSelectionArgs;
    private String mGroupBy;
    private String mHaving;
    private String mOrderBy;

    public SqlWebHistory(Context context, DataSource dataSource, String selection, String[] selectionArgs,
                         String groupBy, String having, String orderBy) {
        super(context);
        mDataSource = dataSource;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mGroupBy = groupBy;
        mHaving = having;
        mOrderBy = orderBy;
    }

    @Override
    protected List<WebHistory> buildList() {
        List<WebHistory> testList = mDataSource.read(mSelection, mSelectionArgs, mGroupBy, mHaving,
                mOrderBy);
        return testList;
    }

    public void insert(WebHistory entity) {
        new InsertTask(this).execute(entity);
    }

    public void update(WebHistory entity) {
        new UpdateTask(this).execute(entity);
    }

    public void delete(WebHistory entity) {
        new DeleteTask(this).execute(entity);
    }

    private class InsertTask extends ContentChangingTask<WebHistory, Void, Void> {
        InsertTask(SqlWebHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(WebHistory... params) {
            mDataSource.insert(params[0]);
            return (null);
        }
    }

    private class UpdateTask extends ContentChangingTask<WebHistory, Void, Void> {
        UpdateTask(SqlWebHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(WebHistory... params) {
            mDataSource.update(params[0]);
            return (null);
        }
    }

    private class DeleteTask extends ContentChangingTask<WebHistory, Void, Void> {
        DeleteTask(SqlWebHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(WebHistory... params) {
            mDataSource.delete(params[0]);
            return (null);
        }
    }
}
