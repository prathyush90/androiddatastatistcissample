package linkart.newproject.com.elementora.DataSources;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import linkart.newproject.com.elementora.Models.CallHistory;
import linkart.newproject.com.elementora.Models.LocationDetails;

/**
 * Created by prathyush on 11/07/16.
 */
public class LocationDataSource extends DataSource<LocationDetails> {

    public static final String TABLE_NAME = "locationhistory";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME_YEAR = "year";
    public static final String COLUMN_NAME_MONTH = "month";
    public static final String COLUMN_NAME_DAY = "day";
    public static final String COLUMN_NAME_HOUR = "hour";
    public static final String COLUMN_NAME_MIN = "min";
    public static final String COLUMN_NAME_SEC = "sec";
    public static final String COLUMN_NAME_LATITUDE = "latitude";
    public static final String COLUMN_NAME_LONGITUDE = "longitude";
    public static final String COLUMN_NAME_MILLI    = "milliseconds";

    public String[] getAllColumns() {
        return new String[] { COLUMN_ID, COLUMN_NAME_YEAR,COLUMN_NAME_MONTH,COLUMN_NAME_DAY,COLUMN_NAME_HOUR,COLUMN_NAME_MIN,COLUMN_NAME_SEC,COLUMN_NAME_LATITUDE,COLUMN_NAME_LONGITUDE,COLUMN_NAME_MILLI};
    }


    // Database creation sql statement
    public static final String CREATE_COMMAND = "create table " + TABLE_NAME
            + "(" + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME_YEAR+" integer,"+COLUMN_NAME_MONTH+" integer,"+COLUMN_NAME_DAY+" integer,"+COLUMN_NAME_HOUR+" integer,"+COLUMN_NAME_MIN+" integer,"+COLUMN_NAME_MILLI+" integer,"+COLUMN_NAME_SEC+" integer,"+COLUMN_NAME_LATITUDE+" double,"+COLUMN_NAME_LONGITUDE+" double   );";
    public LocationDataSource(SQLiteDatabase database) {
        super(database);
    }

    @Override
    public boolean insert(LocationDetails entity) {
        if (entity == null) {
            return false;
        }
        long result = mDatabase.insert(TABLE_NAME, null,
                generateContentValuesFromObject(entity));
        return result != -1;
    }

    private ContentValues generateContentValuesFromObject(LocationDetails entity) {

        if (entity == null) {
            return null;
        }
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_YEAR, entity.getYear());
        values.put(COLUMN_NAME_MONTH, entity.getMonth());
        values.put(COLUMN_NAME_DAY, entity.getDay());
        values.put(COLUMN_NAME_HOUR, entity.getHour());
        values.put(COLUMN_NAME_MIN, entity.getMin());
        values.put(COLUMN_NAME_SEC, entity.getSec());
        values.put(COLUMN_NAME_LATITUDE, entity.getLatitude());
        values.put(COLUMN_NAME_LONGITUDE, entity.getLongitude());
        values.put(COLUMN_NAME_MILLI, entity.getMilliseconds());

        return values;
    }

    @Override
    public boolean delete(LocationDetails entity) {
        if (entity == null) {
            return false;
        }
        int result = mDatabase.delete(TABLE_NAME,
                COLUMN_ID + " = " + entity.getId(), null);
        return result != 0;
    }

    @Override
    public boolean update(LocationDetails entity) {
        if (entity == null) {
            return false;
        }
        int result = mDatabase.update(TABLE_NAME,
                generateContentValuesFromObject(entity), COLUMN_ID + " = "
                        + entity.getId(), null);
        return result != 0;
    }

    @Override
    public List read() {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), null,
                null, null, null, null);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    private Object generateObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        LocationDetails test = new LocationDetails();
        test.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
        test.setYear(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_YEAR)));
        test.setMonth(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_MONTH)));
        test.setDay(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_DAY)));
        test.setHour(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_HOUR)));
        test.setMin(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_MIN)));
        test.setSec(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_SEC)));
        test.setLatitude(cursor.getDouble(cursor.getColumnIndex(COLUMN_NAME_LATITUDE)));
        test.setLongitude(cursor.getDouble(cursor.getColumnIndex(COLUMN_NAME_LONGITUDE)));
        test.setMilliseconds(cursor.getLong(cursor.getColumnIndex(COLUMN_NAME_MILLI)));

        return test;
    }

    @Override
    public List read(String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), selection,
                selectionArgs, groupBy, having, orderBy);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }
}
