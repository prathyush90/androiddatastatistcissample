package linkart.newproject.com.elementora;

/**
 * Created by prathyush on 11/07/16.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import linkart.newproject.com.elementora.DataSources.AppHistoryDataSource;
import linkart.newproject.com.elementora.DataSources.CallHistoryDataSource;
import linkart.newproject.com.elementora.DataSources.LocationDataSource;
import linkart.newproject.com.elementora.DataSources.SmsHistoryDataSource;
import linkart.newproject.com.elementora.DataSources.WebHistoryDataSource;

public class DbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "elementora.db";
    private static final int DATABASE_VERSION = 2;
    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(AppHistoryDataSource.CREATE_COMMAND);
        database.execSQL(CallHistoryDataSource.CREATE_COMMAND);
        database.execSQL(LocationDataSource.CREATE_COMMAND);
        database.execSQL(SmsHistoryDataSource.CREATE_COMMAND);
        database.execSQL(WebHistoryDataSource.CREATE_COMMAND);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + AppHistoryDataSource.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CallHistoryDataSource.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + LocationDataSource.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SmsHistoryDataSource.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + WebHistoryDataSource.TABLE_NAME);
        onCreate(db);
    }
}
