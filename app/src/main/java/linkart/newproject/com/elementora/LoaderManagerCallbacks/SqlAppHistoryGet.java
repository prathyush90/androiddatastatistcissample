package linkart.newproject.com.elementora.LoaderManagerCallbacks;


import android.content.Context;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import java.util.List;

import linkart.newproject.com.elementora.DataSources.AppHistoryDataSource;
import linkart.newproject.com.elementora.DbHelper;
import linkart.newproject.com.elementora.Models.AppHistory;
import linkart.newproject.com.elementora.SqlLiteLoaders.SqlAppHistory;
import linkart.newproject.com.elementora.onLoadInterface;

/**
 * Created by prathyush on 11/07/16.
 */
public class SqlAppHistoryGet implements
        LoaderManager.LoaderCallbacks<List<AppHistory>> {
    private Context mContext;
    private SQLiteDatabase mDatabase;
    private AppHistoryDataSource mDataSource;
    private DbHelper mDbHelper;
    private onLoadInterface mcallback;


    public SqlAppHistoryGet(Context mContext,onLoadInterface loadInterface) {
        this.mContext = mContext;
        mDbHelper = new DbHelper(mContext);
        mDatabase = mDbHelper.getWritableDatabase();
        mDataSource = new AppHistoryDataSource(mDatabase);
        mcallback   = loadInterface;
    }

    @Override
    public Loader<List<AppHistory>> onCreateLoader(int id, Bundle args) {
        String[] selectionargs = null;
        String groupby = null;
        if(args !=null) {
            selectionargs = args.getStringArray("selectionargs");
            groupby = args.getString("groupby");
        }

        SqlAppHistory loader   =   new SqlAppHistory(mContext,mDataSource,null,selectionargs,groupby,null,null);
        return loader;
    }



    @Override
    public void onLoadFinished(Loader<List<AppHistory>> loader, List<AppHistory> data) {
        if(mcallback != null) {
            mcallback.onAppHistory(data);
        }

    }

    @Override
    public void onLoaderReset(Loader<List<AppHistory>> loader) {

    }

    public void onDestroy()
    {
        mDbHelper.close();
        mDatabase.close();
        mDataSource = null;
        mDbHelper = null;
        mDatabase = null;
    }
}
