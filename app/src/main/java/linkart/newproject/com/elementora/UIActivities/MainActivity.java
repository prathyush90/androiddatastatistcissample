package linkart.newproject.com.elementora.UIActivities;

import android.Manifest;
import android.app.ActivityManager;


import android.app.AppOpsManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import linkart.newproject.com.elementora.DataSources.AppHistoryDataSource;
import linkart.newproject.com.elementora.DataSources.CallHistoryDataSource;
import linkart.newproject.com.elementora.DataSources.SmsHistoryDataSource;
import linkart.newproject.com.elementora.DbHelper;
import linkart.newproject.com.elementora.LoaderManagerCallbacks.CallHistoryLoaderManagerCallbacks;
import linkart.newproject.com.elementora.LoaderManagerCallbacks.ChromeHistory;
import linkart.newproject.com.elementora.LoaderManagerCallbacks.OutgoingSms;
import linkart.newproject.com.elementora.LoaderManagerCallbacks.SqlAppHistoryGet;
import linkart.newproject.com.elementora.LoaderManagerCallbacks.SqlCallHistoryGet;
import linkart.newproject.com.elementora.LoaderManagerCallbacks.SqlSmsHistoryGet;
import linkart.newproject.com.elementora.Models.AppHistory;
import linkart.newproject.com.elementora.Models.CallHistory;
import linkart.newproject.com.elementora.Models.RecyclerViewAdapter;
import linkart.newproject.com.elementora.Models.RecyclerViewAdapterSecond;
import linkart.newproject.com.elementora.Models.SmsHistory;
import linkart.newproject.com.elementora.R;
import linkart.newproject.com.elementora.Services.LocationService;
import linkart.newproject.com.elementora.onInitialLoad;
import linkart.newproject.com.elementora.onLoadInterface;

public class MainActivity extends AppCompatActivity implements onLoadInterface,onInitialLoad {

    public static final int CALL_LOADER = 1;
    public static final int BROWSER_LOADER = 2;
    public static final int SMS_LOADER = 3;
    public static final int SQL_CALL_LOADER = 4;
    public static final int SQL_APP_LOADER = 5;
    public static final int SQL_SMS_LOADER = 6;
    private static final int MY_PERMISSIONS_REQUEST_READ_SMS = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 2;
    private static final int MY_PERMISSIONS_REQUEST_READ_CALL_LOG = 3;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 4;
    private boolean flag = true;


    private CallHistoryLoaderManagerCallbacks mCallhistory;
    private ChromeHistory mChromehistory;
    private OutgoingSms outgoingSms;


    private SqlAppHistoryGet sqlAppHistoryGet;
    private SqlCallHistoryGet sqlCallHistoryGet;
    private SqlSmsHistoryGet sqlSmsHistoryGet;
    private long milliseconds = 0l;

    private HashMap<Integer, JSONObject> inputData = new HashMap<>();
    private HashMap<Long, JSONObject> popUpData = new HashMap<>();
    private int count = 3;
    private LoaderManager lm;
    private ArrayList<String> permissionList = new ArrayList<>();
    private LinearLayout popupBar;
    private TextView text;
    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mAdapter;
    private RecyclerViewAdapterSecond mAdapterSecond;
    private TextView cabTime;
    private TextView socialTime;
    private AdapterDataObserver mobserver;
    private AdapterDataObserver mobserver2;
    private ArrayList<Object> queue = new ArrayList<>();
    private Object mLock = new Object();
    private RecyclerView mOnclickRecyclerView;
    private LinearLayout mPopBottom;
    private TextView callTime;

    private HashMap<Integer,Long> userBehaviour = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        popupBar = (LinearLayout) findViewById(R.id.popupBar);
        text = (TextView) findViewById(R.id.text);
        mRecyclerView = (RecyclerView) findViewById(R.id.list);
        cabTime = (TextView) findViewById(R.id.cabTime);
        socialTime = (TextView) findViewById(R.id.socialTime);
        callTime = (TextView) findViewById(R.id.callTime);

        mOnclickRecyclerView = (RecyclerView) findViewById(R.id.listSecond);
        mPopBottom = (LinearLayout) findViewById(R.id.pop_bottom);

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            cabTime.setVisibility(View.GONE);
            socialTime.setVisibility(View.GONE);
            count--;
        }


        mRecyclerView.setVisibility(View.GONE);


        mAdapter = new RecyclerViewAdapter(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP, inputData);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
        mobserver = new RecyclerView.AdapterDataObserver() {

            @Override
            public void onChanged() {
                mAdapter.dataChanged();
            }

        };
        mAdapter.registerAdapterDataObserver(mobserver);

        mAdapterSecond = new RecyclerViewAdapterSecond(popUpData);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext());
        mOnclickRecyclerView.setLayoutManager(mLayoutManager1);
        mOnclickRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mOnclickRecyclerView.setAdapter(mAdapterSecond);
        mobserver2 = new RecyclerView.AdapterDataObserver() {

            @Override
            public void onChanged() {
                mAdapterSecond.dataChanged();
            }

        };
        mAdapterSecond.registerAdapterDataObserver(mobserver2);


        mCallhistory = new CallHistoryLoaderManagerCallbacks(getApplicationContext(), this);
        mChromehistory = new ChromeHistory(getApplicationContext());
        outgoingSms = new OutgoingSms(getApplicationContext(), this);
        sqlAppHistoryGet = new SqlAppHistoryGet(getApplicationContext(), this);
        sqlCallHistoryGet = new SqlCallHistoryGet(getApplicationContext(), this);
        sqlSmsHistoryGet = new SqlSmsHistoryGet(getApplicationContext(), this);

        boolean b = checkPermissions();


        if(b)
        {
            postPermissions();
        }

        mPopBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideUpDown(mPopBottom);
            }
        });


    }

    public void postPermissions() {

        text.setText("Loading required data");
        lm = getSupportLoaderManager();
        if (lm.getLoader(CALL_LOADER) == null || !lm.getLoader(CALL_LOADER).isStarted()) {
            lm.initLoader(CALL_LOADER, null, mCallhistory);
        }
        if (lm.getLoader(BROWSER_LOADER) == null || !lm.getLoader(BROWSER_LOADER).isStarted()) {
            lm.initLoader(BROWSER_LOADER, null, mChromehistory);
        }
        if (lm.getLoader(SMS_LOADER) == null || !lm.getLoader(SMS_LOADER).isStarted()) {

            lm.initLoader(SMS_LOADER, null, outgoingSms);
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && flag) {
            new getAppsHistory().execute();
        }

        if (!LocationService.mIsRunning) {
            startService(new Intent(this, LocationService.class));
        }


    }

    public boolean checkPermissions() {

        text.setText("Waiting for permisssions");

        String permissionsms = Manifest.permission.READ_SMS;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_SMS)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_SMS},
                        MY_PERMISSIONS_REQUEST_READ_SMS);
                return false;


            }
        }

        String permissioncontact = Manifest.permission.READ_CONTACTS;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                return false;


            }
        }


        String permissioncalllog = Manifest.permission.READ_CALL_LOG;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_CALL_LOG)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        MY_PERMISSIONS_REQUEST_READ_CALL_LOG);
                return false;


            }
        }

        String permissionlocation = Manifest.permission.ACCESS_FINE_LOCATION;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                return false;


            }
        }


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AppOpsManager appOps = (AppOpsManager) this
                    .getSystemService(Context.APP_OPS_SERVICE);
            int mode = 0;
            mode = appOps.checkOpNoThrow("android:get_usage_stats",
                    android.os.Process.myUid(), this.getPackageName());
            //boolean granted = (mode == AppOpsManager.MODE_ALLOWED);
            if (!(mode == AppOpsManager.MODE_ALLOWED)) {
                startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                return false;

            }
        }
        return true;

    }

    @Override
    protected void onResume() {
        super.onResume();


        boolean b = checkPermissions();


        if(b)
        {
            postPermissions();
        }
    }


    public void getCallHourWise() {

        Bundle callArgs = new Bundle();
        callArgs.putString("groupby", CallHistoryDataSource.COLUMN_NAME_YEAR + "," + CallHistoryDataSource.COLUMN_NAME_MONTH + "," + CallHistoryDataSource.COLUMN_NAME_DAY+"," + CallHistoryDataSource.COLUMN_NAME_HOUR );
        callArgs.putString("orderby",CallHistoryDataSource.COLUMN_NAME_MILLI+ "  DESC,"+CallHistoryDataSource.COLUMN_NAME_DURATION+ "  DESC");
        sqlCallHistoryGet.setFlag(true);
        lm.restartLoader(SQL_CALL_LOADER, callArgs, sqlCallHistoryGet);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sqlSmsHistoryGet.onDestroy();
        sqlCallHistoryGet.onDestroy();
        sqlAppHistoryGet.onDestroy();
        lm.destroyLoader(CALL_LOADER);
        lm.destroyLoader(SMS_LOADER);
        lm.destroyLoader(BROWSER_LOADER);
        lm.destroyLoader(SQL_SMS_LOADER);
        lm.destroyLoader(SQL_CALL_LOADER);
        lm.destroyLoader(SQL_APP_LOADER);
        mAdapter.unregisterAdapterDataObserver(mobserver);
    }

    @Override
    public void onCallHistory(List<CallHistory> callHistoryList) {

        queue.add(callHistoryList);
        addsynchronously();

    }

    @Override
    public void onSmsHistory(List<SmsHistory> callHistoryList) {

        queue.add(callHistoryList);
        addsynchronously();

    }

    @Override
    public void onAppHistory(List<AppHistory> callHistoryList) {

        queue.add(callHistoryList);
        addsynchronously();


    }

    @Override
    public void onCallHourWise(List<CallHistory> callHistoryList) {

        for (int i = 0; i < callHistoryList.size(); i++) {
            //Calendar calendar = Calendar.getInstance();
            CallHistory callHistory = callHistoryList.get(i);

            long hash = hashCode(callHistory.getYear(), callHistory.getMonth(), callHistory.getDay(), callHistory.getHour());
            JSONObject data = popUpData.get(hash);
            if (data == null) {
                data = new JSONObject();
                try {
                    data.put("calltime", Integer.valueOf(callHistory.getDuration()));
                    if (callHistory.getType() == 1) {
                        data.put("incomingcalls", 1);
                    } else if (callHistory.getType() == 2) {
                        data.put("outgoingcalls", 1);
                    }
                    ArrayList<String> numbers = new ArrayList<>();
                    numbers.add(callHistory.getFormattednumber());
                    data.put("numbers", numbers);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                int calltime = 0;
                try {
                    if (data.has("calltime")) {
                        calltime = data.getInt("calltime");
                    }
                    data.put("calltime", calltime + Integer.valueOf(callHistory.getDuration()));
                    int numincomingcalls = 0;
                    int outgoingcalls = 0;
                    if (data.has("incomingcalls")) {
                        numincomingcalls = data.getInt("incomingcalls");
                    }
                    if (data.has("outgoingcalls")) {
                        outgoingcalls = data.getInt("outgoingcalls");
                    }
                    if (callHistory.getType() == 1) {
                        data.put("incomingcalls", numincomingcalls + 1);
                    } else if (callHistory.getType() == 2) {
                        data.put("outgoingcalls", outgoingcalls + 1);
                    }
                    Set<String> numberswithoutduplicates = new HashSet<>();
                    ArrayList<String> presentNumbers = (ArrayList<String>) data.get("numbers");
                    numberswithoutduplicates.addAll(presentNumbers);
                    numberswithoutduplicates.add(callHistory.getFormattednumber());
                    ArrayList<String> finalList = new ArrayList<>(numberswithoutduplicates);
                    data.put("numbers", finalList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            //calendar.set(callHistory.getYear(), callHistory.getMonth(), callHistory.getDay(), callHistory.getHour(), 0);
            try {
                data.put("milliseconds", callHistory.getMilliseconds());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            popUpData.put(hash, data);


        }
        popupBar.setVisibility(View.GONE);
        mAdapterSecond.notifyDataSetChanged();
        if(popUpData.size() == 0)
        {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, "No hour wise data to show", Toast.LENGTH_LONG).show();
                }
            });

        }

    }

    public void callHistorySync(List<CallHistory> callHistoryList) {
        for (int i = 0; i < callHistoryList.size(); i++) {
            Calendar calendar = Calendar.getInstance();
            CallHistory callHistory = callHistoryList.get(i);
            calendar.set(callHistory.getYear(), callHistory.getMonth(), callHistory.getDay());
            int hash = hashCode(callHistory.getYear(), callHistory.getMonth(), callHistory.getDay());
            int hashmonthly = hashCode(callHistory.getYear(),callHistory.getMonth());
            JSONObject data = inputData.get(hash);
            Long tttime  = 0l;
            if(userBehaviour.containsKey(hashmonthly))
            {
                tttime = userBehaviour.get(hashmonthly)+Long.parseLong(callHistory.getDuration());
            }else
            {
                tttime = Long.parseLong(callHistory.getDuration());
            }
            if (data == null) {
                data = new JSONObject();
                try {

                    data.put("calltime", Integer.valueOf(callHistory.getDuration()));
                    data.put("totaltime", Integer.valueOf(callHistory.getDuration()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                int calltime = 0;
                try {
                    if (data.has("calltime")) {
                        calltime = data.getInt("calltime");
                    }
                    data.put("calltime", calltime + Integer.valueOf(callHistory.getDuration()));
                    int totaltime = 0;
                    if (data.has("totaltime")) {
                        totaltime = data.getInt("totaltime");
                    }

                    data.put("totaltime", totaltime + Integer.valueOf(callHistory.getDuration()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            calendar.set(callHistory.getYear(), callHistory.getMonth(), callHistory.getDay());
            try {
                data.put("milliseconds", calendar.getTimeInMillis());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            inputData.put(hash, data);
            userBehaviour.put(hashmonthly,tttime);

        }

        count--;

        if (count == 0) {
            count = 3;
            popupBar.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mAdapter.notifyDataSetChanged();
            callTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getCallHourWise();
                    slideUpDown(mPopBottom);

                }
            });
            getUserBehaviour();
            if(inputData.size() ==0) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "No smslogs,call logs,app usage data to show", Toast.LENGTH_LONG).show();
                    }
                });
            }

        }
    }

    public void appHistorySync(List<AppHistory> callHistoryList) {

        for (int i = 0; i < callHistoryList.size(); i++) {
            Calendar calendar = Calendar.getInstance();
            AppHistory callHistory = callHistoryList.get(i);
            int hash = hashCode(callHistory.getYear(), callHistory.getMonth(), callHistory.getDay());
            int hashmonthly = hashCode(callHistory.getYear(),callHistory.getMonth());
            JSONObject data = inputData.get(hash);
            Long tttime  = 0l;
            if(userBehaviour.containsKey(hashmonthly))
            {
                tttime = userBehaviour.get(hashmonthly)+callHistory.getDuration();
            }else
            {
                tttime = callHistory.getDuration();
            }
            if (data == null) {
                data = new JSONObject();
                try {
                    if (callHistory.getType_app().equals("social")) {

                        data.put("social", (int) callHistory.getDuration());
                    } else if (callHistory.getType_app().equals("cabs")) {

                        data.put("cabs", (int) callHistory.getDuration());
                    }
                    data.put("totaltime", (int) callHistory.getDuration());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

                try {
                    if (callHistory.getType_app().equals("social")) {
                        int social_count = 0;
                        if (data.has("social")) {
                            social_count = data.getInt("social");
                        }
                        data.put("social", social_count + (int) callHistory.getDuration());
                    } else if (callHistory.getType_app().equals("cabs")) {
                        int social_count = 0;
                        if (data.has("cabs")) {
                            social_count = data.getInt("cabs");
                        }
                        data.put("cabs", social_count + (int) callHistory.getDuration());
                    }
                    int count = 0;
                    if (data.has("totaltime")) {
                        count = data.getInt("totaltime");
                    }
                    data.put("totaltime", count + (int) callHistory.getDuration());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            calendar.set(callHistory.getYear(), callHistory.getMonth(), callHistory.getDay());
            try {
                data.put("milliseconds", calendar.getTimeInMillis());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            inputData.put(hash, data);
            userBehaviour.put(hashmonthly,tttime);

        }

        count--;
        if (count == 0) {
            count = 3;
            popupBar.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mAdapter.notifyDataSetChanged();
            callTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mRecyclerView.setVisibility(View.GONE);
                    getCallHourWise();
                    slideUpDown(mPopBottom);
                }
            });
            getUserBehaviour();
            if(inputData.size() == 0)
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this,"No smslogs,call logs,app usage data to show",Toast.LENGTH_LONG).show();
                    }
                });

            }


        }

    }

    private void getUserBehaviour() {

        Runnable r = new Runnable() {
            @Override
            public void run() {

                Set<Integer>keyset = userBehaviour.keySet();
                ArrayList<Integer>keysetlist = new ArrayList<>(keyset);
                Long total = 0l;

                for(int i=0;i<keysetlist.size();i++)
                {
                    total   += userBehaviour.get(keysetlist.get(i));
                }
                long average  = 0l;
                if(keysetlist.size() != 0)
                {
                    average  =  (total/keysetlist.size());
                }

                String text = "";

                if(average > 8l*3600l)
                {
                    text = "This user is high mobile addict";

                }else if(average >4l*3600l)
                {
                    text = "This user is moderately addict";

                }else if(average <4l*3600l)
                {
                    text = "This user is not a mobile addict";
                }

                final String finalText = text;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, finalText,Toast.LENGTH_LONG).show();
                    }
                });

            }
        };
        new Thread(r).start();

    }

    public void smsHistorySync(List<SmsHistory> callHistoryList) {
        for (int i = 0; i < callHistoryList.size(); i++) {
            Calendar calendar = Calendar.getInstance();
            SmsHistory callHistory = callHistoryList.get(i);
            int hash = hashCode(callHistory.getYear(), callHistory.getMonth(), callHistory.getDay());
            JSONObject data = inputData.get(hash);
            if (data == null) {
                data = new JSONObject();
                try {
                    data.put("numofsms", 1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                int calltime = 0;
                try {
                    if (data.has("numofsms")) {
                        calltime = data.getInt("numofsms");
                    }
                    data.put("numofsms", calltime + 1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            calendar.set(callHistory.getYear(), callHistory.getMonth(), callHistory.getDay());
            try {
                data.put("milliseconds", calendar.getTimeInMillis());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            inputData.put(hash, data);

        }
        count--;
        if (count == 0) {
            count = 3;
            popupBar.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mAdapter.notifyDataSetChanged();
            callTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getCallHourWise();
                    slideUpDown(mPopBottom);
                }
            });

            if(inputData.size() == 0)
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this,"No smslogs,call logs,app usage data to show",Toast.LENGTH_LONG).show();
                    }
                });
            }
            getUserBehaviour();

        }
    }

    public void addsynchronously() {
        synchronized (mLock) {
            for (int i = 0; i < queue.size(); i++) {
                List<Object> m = (List<Object>) queue.get(i);
                if(m.size() == 0)
                {
                    count--;
                    if (count == 0) {
                        count = 3;
                        popupBar.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mAdapter.notifyDataSetChanged();
                        callTime.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getCallHourWise();
                                slideUpDown(mPopBottom);
                            }
                        });
                        getUserBehaviour();
                        if(inputData.size() == 0)
                        {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this,"No smslogs,call logs,app usage data to show",Toast.LENGTH_LONG).show();
                                }
                            });
                        }

                    }
                    queue.remove(i);
                    return;
                }
                if (m.size() > 0 && m.get(0) instanceof AppHistory) {
                    appHistorySync((List<AppHistory>) queue.get(i));
                    queue.remove(i);
                } else if (m.size() > 0 && m.get(0) instanceof SmsHistory) {
                    smsHistorySync((List<SmsHistory>) queue.get(i));
                    queue.remove(i);
                } else if (m.size() > 0 && m.get(0) instanceof CallHistory) {
                    callHistorySync((List<CallHistory>) queue.get(i));
                    queue.remove(i);
                }
            }
        }
    }

    @Override
    public void onCallHistory() {

        Bundle callArgs = new Bundle();
        callArgs.putString("groupby", CallHistoryDataSource.COLUMN_NAME_YEAR + "," + CallHistoryDataSource.COLUMN_NAME_MONTH + "," + CallHistoryDataSource.COLUMN_NAME_DAY);
        callArgs.putString("orderby", CallHistoryDataSource.COLUMN_NAME_MILLI + "  DESC");
        lm.initLoader(SQL_CALL_LOADER, callArgs, sqlCallHistoryGet);





    }

    @Override
    public void onSmsHistory() {

        Bundle smsArgs = new Bundle();
        smsArgs.putString("groupby", SmsHistoryDataSource.COLUMN_NAME_YEAR + "," + SmsHistoryDataSource.COLUMN_NAME_MONTH + "," + SmsHistoryDataSource.COLUMN_NAME_DAY);
        smsArgs.putString("orderby", SmsHistoryDataSource.COLUMN_NAME_MILLI + "  DESC");
        lm.initLoader(SQL_SMS_LOADER, smsArgs, sqlSmsHistoryGet);

    }


    private class getAppsHistory extends AsyncTask<Void, Void, Void> {
        private List<UsageStats> queryUsageStats;
        private List<ActivityManager.RunningTaskInfo> mRunningTasks;

        @Override
        protected Void doInBackground(Void... params) {
            flag = false;
            final UsageStatsManager usageStatsManager = (UsageStatsManager) MainActivity.this.getSystemService(Context.USAGE_STATS_SERVICE);// Context.USAGE_STATS_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {


                Calendar calendar = Calendar.getInstance();
                long currMilliseconds = calendar.getTimeInMillis();
                SharedPreferences mPreference = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                milliseconds    = mPreference.getLong("appMilliseconds",0l);

                DbHelper helper = new DbHelper(MainActivity.this);
                SQLiteDatabase database = helper.getWritableDatabase();
                AppHistoryDataSource mDataSource = new AppHistoryDataSource(database);
                String[] social = {"com.facebook.katana", "com.twitter.android", "com.google.android.apps.plus", "com.ninegag.android.app"};
                String[] cabs = {"com.olacabs.customer", "com.ubercab"};


                    queryUsageStats = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, milliseconds, currMilliseconds);


                    for (int i = 0; i < queryUsageStats.size(); i++) {
                        AppHistory callHistory = new AppHistory();
                        UsageStats usageStats = queryUsageStats.get(i);
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(usageStats.getLastTimeUsed());
                        callHistory.setMilliseconds(milliseconds);
                        callHistory.setYear(cal.get(Calendar.YEAR));
                        callHistory.setMonth(cal.get(Calendar.MONTH));
                        callHistory.setDay(cal.get(Calendar.DAY_OF_MONTH));
                        callHistory.setHour(cal.get(Calendar.HOUR));
                        callHistory.setMin(cal.get(Calendar.MINUTE));
                        callHistory.setSec(cal.get(Calendar.SECOND));
                        callHistory.setPackagename(usageStats.getPackageName());
                        callHistory.setDuration((usageStats.getTotalTimeInForeground()/1000));
                        if (Arrays.asList(social).contains(usageStats.getPackageName())) {
                            callHistory.setType_app("social");
                        } else if (Arrays.asList(cabs).contains(usageStats.getPackageName())) {
                            callHistory.setType_app("cabs");
                        } else {
                            callHistory.setType_app("none");
                        }
                        mDataSource.insert(callHistory);


                    milliseconds = currMilliseconds;
                }
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
                editor.putLong("appMilliseconds",milliseconds);
                editor.commit();
                database.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            Bundle callArgs = new Bundle();
            callArgs.putString("groupby", AppHistoryDataSource.COLUMN_NAME_YEAR + "," + AppHistoryDataSource.COLUMN_NAME_MONTH + "," + AppHistoryDataSource.COLUMN_NAME_DAY);
            callArgs.putString("orderby", AppHistoryDataSource.COLUMN_NAME_MILLI + "  DESC");
            lm.initLoader(SQL_APP_LOADER, callArgs, sqlAppHistoryGet);

        }


    }

    public int hashCode(int year, int month, int day) {
        return ((year << 4) | month) << 5 | day;
    }

    public long hashCode(int year, int month, int day, int hour) {
        return (((year << 4) | month) << 5 | day) << 5 | hour;
    }

    public int hashCode(int year, int month) {
        return (((year << 4) | month) << 5 );
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        boolean b = checkPermissions();


        if(b)
        {
            postPermissions();
        }
    }

    public void slideUpDown(final View view) {
        if (view.getVisibility() == View.GONE) {
            // Show the panel
            Animation bottomUp = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_up);

            view.startAnimation(bottomUp);
            view.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);

        } else {
            // Hide the Panel
            Animation bottomDown = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_down);

            view.startAnimation(bottomDown);
            view.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);

        }
    }
}
