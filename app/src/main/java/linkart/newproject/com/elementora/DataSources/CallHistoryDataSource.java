package linkart.newproject.com.elementora.DataSources;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import linkart.newproject.com.elementora.Models.AppHistory;
import linkart.newproject.com.elementora.Models.CallHistory;

/**
 * Created by prathyush on 11/07/16.
 */
public class CallHistoryDataSource extends DataSource<CallHistory> {

    public static final String TABLE_NAME = "callhistory";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME_YEAR = "year";
    public static final String COLUMN_NAME_MONTH = "month";
    public static final String COLUMN_NAME_DAY = "day";
    public static final String COLUMN_NAME_HOUR = "hour";
    public static final String COLUMN_NAME_MIN = "min";
    public static final String COLUMN_NAME_SEC = "sec";
    public static final String COLUMN_NAME_NUMBER = "formattednumber";
    public static final String COLUMN_NAME_DURATION = "duration";
    public static final String COLUMN_NAME_TYPE = "type";
    public static final String COLUMN_NAME_MILLI    = "milliseconds";

    public String[] getAllColumns() {
        return new String[] { COLUMN_ID, COLUMN_NAME_YEAR,COLUMN_NAME_MONTH,COLUMN_NAME_DAY,COLUMN_NAME_HOUR,COLUMN_NAME_MIN,COLUMN_NAME_SEC,COLUMN_NAME_NUMBER,COLUMN_NAME_DURATION,COLUMN_NAME_TYPE,COLUMN_NAME_MILLI};
    }


    // Database creation sql statement
    public static final String CREATE_COMMAND = "create table " + TABLE_NAME
            + "(" + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME_YEAR+" integer,"+COLUMN_NAME_MONTH+" integer,"+COLUMN_NAME_DAY+" integer,"+COLUMN_NAME_HOUR+" integer,"+COLUMN_NAME_MIN+" integer,"+COLUMN_NAME_SEC+" integer,"+COLUMN_NAME_MILLI+" integer,"+COLUMN_NAME_NUMBER+" text,"+COLUMN_NAME_TYPE+" integer,"+COLUMN_NAME_DURATION+" text   );";
    public CallHistoryDataSource(SQLiteDatabase database) {
        super(database);
    }

    @Override
    public boolean insert(CallHistory entity) {
        if (entity == null) {
            return false;
        }
        long result = mDatabase.insert(TABLE_NAME, null,
                generateContentValuesFromObject(entity));
        return result != -1;
    }

    private ContentValues generateContentValuesFromObject(CallHistory entity) {

        if (entity == null) {
            return null;
        }
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_YEAR, entity.getYear());
        values.put(COLUMN_NAME_MONTH, entity.getMonth());
        values.put(COLUMN_NAME_DAY, entity.getDay());
        values.put(COLUMN_NAME_HOUR, entity.getHour());
        values.put(COLUMN_NAME_MIN, entity.getMin());
        values.put(COLUMN_NAME_SEC, entity.getSec());
        values.put(COLUMN_NAME_NUMBER, entity.getFormattednumber());
        values.put(COLUMN_NAME_DURATION, entity.getDuration());
        values.put(COLUMN_NAME_TYPE, entity.getType());
        values.put(COLUMN_NAME_MILLI, entity.getMilliseconds());
        return values;
    }


    @Override
    public boolean delete(CallHistory entity) {
        if (entity == null) {
            return false;
        }
        int result = mDatabase.delete(TABLE_NAME,
                COLUMN_ID + " = " + entity.getId(), null);
        return result != 0;
    }

    @Override
    public boolean update(CallHistory entity) {
        if (entity == null) {
            return false;
        }
        int result = mDatabase.update(TABLE_NAME,
                generateContentValuesFromObject(entity), COLUMN_ID + " = "
                        + entity.getId(), null);
        return result != 0;
    }

    @Override
    public List read() {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), null,
                null, null, null, null);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    @Override
    public List read(String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), selection,
                selectionArgs, groupBy, having, orderBy);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    public CallHistory generateObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        CallHistory test = new CallHistory();
        test.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
        test.setYear(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_YEAR)));
        test.setMonth(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_MONTH)));
        test.setDay(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_DAY)));
        test.setHour(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_HOUR)));
        test.setMin(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_MIN)));
        test.setSec(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_SEC)));
        test.setFormattednumber(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_NUMBER)));
        test.setDuration(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_DURATION)));
        test.setType(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_TYPE)));
        test.setMilliseconds(cursor.getLong(cursor.getColumnIndex(COLUMN_NAME_MILLI)));

        return test;
    }
}
