package linkart.newproject.com.elementora.Models;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;

import linkart.newproject.com.elementora.R;

/**
 * Created by prathyush on 12/07/16.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    private boolean flag = false;


    private HashMap<Integer,JSONObject> inputData = new HashMap<>();
    private ArrayList<Integer>keysetsorted;

    public RecyclerViewAdapter(boolean flag, HashMap<Integer, JSONObject> inputData) {
        this.flag = flag;
        this.inputData = inputData;
        Set<Integer> keyset = inputData.keySet();
        keysetsorted = new ArrayList<>(keyset);
        Collections.sort(keysetsorted, new Comparator<Integer>() {
            @Override
            public int compare(Integer l1, Integer l2) {

                return (l1 > l2 ? -1 : (l1 == l2 ? 0 : 1));
            }
        });

    }

    public void dataChanged()
    {
        Set<Integer> keyset = inputData.keySet();
        keysetsorted = new ArrayList<>(keyset);
        Collections.sort(keysetsorted, new Comparator<Integer>() {
            @Override
            public int compare(Integer l1, Integer l2) {

                return (l1 > l2 ? -1 : (l1 == l2 ? 0 : 1));
            }
        });
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

          Integer milliseconds    =  keysetsorted.get(position);


          JSONObject data      =  inputData.get(milliseconds);
          Calendar cal         = Calendar.getInstance();
        try {
            cal.setTimeInMillis(data.getLong("milliseconds"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String date          = String.valueOf(cal.get(Calendar.DAY_OF_MONTH))+"/"+String.valueOf(cal.get(Calendar.MONTH))+"/"+String.valueOf(cal.get(Calendar.YEAR));

        holder.sn0.setText(String.valueOf(position+1));
          holder.date.setText(date);


            if(!flag)
            {
                holder.cabtime.setVisibility(View.GONE);
                holder.socialtime.setVisibility(View.GONE);
            }else
            {
                try {
                    holder.cabtime.setText(String.valueOf(data.getInt("cabtime")));

                }catch (JSONException e)
                {
                    holder.cabtime.setText("0");
                }
                try {
                    holder.socialtime.setText(String.valueOf(data.getInt("socialtime")));
                }catch (JSONException e)
                {
                    holder.socialtime.setText("0");
                }
            }
            try {
                holder.calltime.setText(String.valueOf(data.getInt("calltime")));
            }catch (JSONException e)
            {
                holder.calltime.setText("0");
            }
            try {
                holder.totaltime.setText(String.valueOf(data.getInt("totaltime")));
            }catch (JSONException e)
            {
                holder.totaltime.setText("0");
            }




    }



    @Override
    public int getItemCount() {
        return inputData.keySet().size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView sn0;
        private TextView date;
        private TextView calltime;
        private TextView cabtime;
        private TextView socialtime;
        private TextView totaltime;

        public MyViewHolder(View itemView) {
            super(itemView);

            sn0  = (TextView) itemView.findViewById(R.id.sno);
            date  = (TextView) itemView.findViewById(R.id.date);
            calltime  = (TextView) itemView.findViewById(R.id.calltime);
            cabtime  = (TextView) itemView.findViewById(R.id.cabtime);
            socialtime  = (TextView) itemView.findViewById(R.id.socialtime);
            totaltime  = (TextView) itemView.findViewById(R.id.totaltime);

        }
    }
}
