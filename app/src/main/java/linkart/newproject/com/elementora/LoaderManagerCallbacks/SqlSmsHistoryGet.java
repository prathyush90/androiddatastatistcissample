package linkart.newproject.com.elementora.LoaderManagerCallbacks;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import java.util.List;

import linkart.newproject.com.elementora.DataSources.CallHistoryDataSource;
import linkart.newproject.com.elementora.DataSources.SmsHistoryDataSource;
import linkart.newproject.com.elementora.DbHelper;
import linkart.newproject.com.elementora.Models.AppHistory;
import linkart.newproject.com.elementora.Models.SmsHistory;
import linkart.newproject.com.elementora.SqlLiteLoaders.SqlAppHistory;
import linkart.newproject.com.elementora.SqlLiteLoaders.SqlSmsHistory;
import linkart.newproject.com.elementora.onLoadInterface;

/**
 * Created by prathyush on 11/07/16.
 */
public class SqlSmsHistoryGet implements
        LoaderManager.LoaderCallbacks<List<SmsHistory>> {
    private Context mContext;
    private SQLiteDatabase mDatabase;
    private SmsHistoryDataSource mDataSource;
    private DbHelper mDbHelper;
    private onLoadInterface mcallback;


    public SqlSmsHistoryGet(Context mContext,onLoadInterface loadInterface) {
        this.mContext = mContext;
        mDbHelper = new DbHelper(mContext);
        mDatabase = mDbHelper.getWritableDatabase();
        mDataSource = new SmsHistoryDataSource(mDatabase);
        mcallback   = loadInterface;
    }

    @Override
    public Loader<List<SmsHistory>> onCreateLoader(int id, Bundle args) {
        String[] selectionargs = null;
        String groupby = null;
        String selection = null;
        String orderby   = null;
        if(args !=null) {
            selection     = args.getString("selection");
            selectionargs = args.getStringArray("selectionargs");
            groupby = args.getString("groupby");
            orderby = args.getString("orderby");
        }

        SqlSmsHistory loader   =   new SqlSmsHistory(mContext,mDataSource,selection,selectionargs,groupby,null,orderby);
        return loader;
    }



    @Override
    public void onLoadFinished(Loader<List<SmsHistory>> loader, List<SmsHistory> data) {

        if(mcallback != null) {
            mcallback.onSmsHistory(data);
        }


    }

    @Override
    public void onLoaderReset(Loader<List<SmsHistory>> loader) {

    }

    public void onDestroy()
    {
        mDbHelper.close();
        mDatabase.close();
        mDataSource = null;
        mDbHelper = null;
        mDatabase = null;
    }
}
