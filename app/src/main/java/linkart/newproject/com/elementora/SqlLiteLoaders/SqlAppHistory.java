package linkart.newproject.com.elementora.SqlLiteLoaders;

import android.content.Context;

import java.util.List;

import linkart.newproject.com.elementora.DataSources.DataSource;
import linkart.newproject.com.elementora.Models.AppHistory;
import linkart.newproject.com.elementora.SqlLiteReaderHelper.AbstractDataLoader;
import linkart.newproject.com.elementora.SqlLiteReaderHelper.ContentChangingTask;

/**
 * Created by prathyush on 11/07/16.
 */
public class SqlAppHistory extends AbstractDataLoader<List<AppHistory>> {

    private DataSource<AppHistory> mDataSource;
    private String mSelection;
    private String[] mSelectionArgs;
    private String mGroupBy;
    private String mHaving;
    private String mOrderBy;

    public SqlAppHistory(Context context, DataSource dataSource, String selection, String[] selectionArgs,
                         String groupBy, String having, String orderBy) {
        super(context);
        mDataSource = dataSource;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mGroupBy = groupBy;
        mHaving = having;
        mOrderBy = orderBy;
    }

    @Override
    protected List<AppHistory> buildList() {
        List<AppHistory> testList = mDataSource.read(mSelection, mSelectionArgs, mGroupBy, mHaving,
                mOrderBy);
        return testList;
    }

    public void insert(AppHistory entity) {
        new InsertTask(this).execute(entity);
    }

    public void update(AppHistory entity) {
        new UpdateTask(this).execute(entity);
    }

    public void delete(AppHistory entity) {
        new DeleteTask(this).execute(entity);
    }

    private class InsertTask extends ContentChangingTask<AppHistory, Void, Void> {
        InsertTask(SqlAppHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(AppHistory... params) {
            mDataSource.insert(params[0]);
            return (null);
        }
    }

    private class UpdateTask extends ContentChangingTask<AppHistory, Void, Void> {
        UpdateTask(SqlAppHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(AppHistory... params) {
            mDataSource.update(params[0]);
            return (null);
        }
    }

    private class DeleteTask extends ContentChangingTask<AppHistory, Void, Void> {
        DeleteTask(SqlAppHistory loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(AppHistory... params) {
            mDataSource.delete(params[0]);
            return (null);
        }
    }
}
