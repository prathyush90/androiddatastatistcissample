package linkart.newproject.com.elementora.LoaderManagerCallbacks;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import java.util.List;

import linkart.newproject.com.elementora.DataSources.AppHistoryDataSource;
import linkart.newproject.com.elementora.DataSources.CallHistoryDataSource;
import linkart.newproject.com.elementora.DbHelper;
import linkart.newproject.com.elementora.Models.AppHistory;
import linkart.newproject.com.elementora.Models.CallHistory;
import linkart.newproject.com.elementora.SqlLiteLoaders.SqlAppHistory;
import linkart.newproject.com.elementora.SqlLiteLoaders.SqlCallHistory;
import linkart.newproject.com.elementora.onLoadInterface;

/**
 * Created by prathyush on 11/07/16.
 */
public class SqlCallHistoryGet implements
        LoaderManager.LoaderCallbacks<List<CallHistory>> {
    private Context mContext;
    private SQLiteDatabase mDatabase;
    private CallHistoryDataSource mDataSource;
    private DbHelper mDbHelper;
    private onLoadInterface mcallback;
    private boolean flag = false;

    public SqlCallHistoryGet(Context mContext,onLoadInterface loadInterface) {
        this.mContext = mContext;
        mDbHelper = new DbHelper(mContext);
        mDatabase = mDbHelper.getWritableDatabase();
        mDataSource = new CallHistoryDataSource(mDatabase);
        mcallback   = loadInterface;
    }

    @Override
    public Loader<List<CallHistory>> onCreateLoader(int id, Bundle args) {
        String[] selectionargs = null;
        String groupby = null;
        String selection = null;
        String orderby   = null;
        if(args !=null) {
            selection     = args.getString("selection");
            selectionargs = args.getStringArray("selectionargs");
            groupby = args.getString("groupby");
            orderby = args.getString("orderby");
        }

        SqlCallHistory loader   =   new SqlCallHistory(mContext,mDataSource,selection,selectionargs,groupby,null,orderby);
        return loader;
    }



    @Override
    public void onLoadFinished(Loader<List<CallHistory>> loader, List<CallHistory> data) {


        if(mcallback != null && !flag) {
            mcallback.onCallHistory(data);
        }else
        {
           mcallback.onCallHourWise(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<CallHistory>> loader) {

    }

    public void onDestroy()
    {
        mDbHelper.close();
        mDatabase.close();
        mDataSource = null;
        mDbHelper = null;
        mDatabase = null;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
