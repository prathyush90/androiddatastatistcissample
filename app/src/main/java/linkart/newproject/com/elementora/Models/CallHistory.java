package linkart.newproject.com.elementora.Models;

/**
 * Created by prathyush on 11/07/16.
 */
public class CallHistory {

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getSec() {
        return sec;
    }

    public void setSec(int sec) {
        this.sec = sec;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getFormattednumber() {
        return formattednumber;
    }

    public void setFormattednumber(String formattednumber) {
        this.formattednumber = formattednumber;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    int year;
    int month;
    int day;
    int hour;
    int min;
    int sec;
    int type;
    String formattednumber;
    String duration;

    public long getMilliseconds() {
        return milliseconds;
    }

    public void setMilliseconds(long milliseconds) {
        this.milliseconds = milliseconds;
    }

    long milliseconds;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    int id;



}
