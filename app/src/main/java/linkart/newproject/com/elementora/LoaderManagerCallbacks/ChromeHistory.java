package linkart.newproject.com.elementora.LoaderManagerCallbacks;

import android.content.Context;


import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;


import java.util.Calendar;


import linkart.newproject.com.elementora.DataSources.CallHistoryDataSource;
import linkart.newproject.com.elementora.DataSources.WebHistoryDataSource;
import linkart.newproject.com.elementora.DbHelper;

import linkart.newproject.com.elementora.Models.CallHistory;
import linkart.newproject.com.elementora.Models.WebHistory;
import linkart.newproject.com.elementora.UIActivities.MainActivity;

/**
 * Created by prathyush on 11/07/16.
 */
public class ChromeHistory implements LoaderManager.LoaderCallbacks<Cursor> {
private Context mContext;
    private int count = 0;

public ChromeHistory(Context applicationContext) {

        this.mContext = applicationContext;
    SharedPreferences mPreference = PreferenceManager.getDefaultSharedPreferences(mContext);
    count    = mPreference.getInt("chromeCount",0);
        }


@Override
public Loader onCreateLoader(int id, Bundle args) {
        return new CursorLoader(mContext, Uri.parse("content://com.android.chrome.browser/history"),
        null, null, null, null);
        }

@Override
public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        switch (loader.getId())
        {
        case MainActivity.BROWSER_LOADER :
            new backGroundTask().execute(data);
        break;
        }

        }



@Override
public void onLoaderReset(Loader loader) {

        }

    public class backGroundTask extends AsyncTask<Cursor,Void,Void>
    {

        @Override
        protected Void doInBackground(Cursor... params) {
            DbHelper helper = new DbHelper(mContext);
            SQLiteDatabase database = helper.getWritableDatabase();
            Cursor data = params[0];
            if(data != null && data.getCount() != count)
            {
                WebHistoryDataSource mDataSource = new WebHistoryDataSource(database);

                while (data.moveToNext() && !data.isClosed())
                {
                    WebHistory callHistory = new WebHistory();
                    Calendar   calendar    = Calendar.getInstance();
                    calendar.setTimeInMillis(data.getLong(data.getColumnIndex("date")));
                    callHistory.setYear(calendar.get(Calendar.YEAR));
                    callHistory.setMonth(calendar.get(Calendar.MONTH));
                    callHistory.setDay(calendar.get(Calendar.DAY_OF_MONTH));
                    callHistory.setHour(calendar.get(Calendar.HOUR));
                    callHistory.setMin(calendar.get(Calendar.MINUTE));
                    callHistory.setSec(calendar.get(Calendar.SECOND));
                    callHistory.setTitle(data.getString(data.getColumnIndex("title")));
                    callHistory.setBookmark(data.getString(data.getColumnIndex("bookmark")));
                    callHistory.setCreated(data.getString(data.getColumnIndex("created")));
                    callHistory.setUrl(data.getString(data.getColumnIndex("url")));
                    callHistory.setMilliseconds(calendar.getTimeInMillis());


                    mDataSource.insert(callHistory);
                }
                if(data != null) {
                    count = data.getCount();
                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
                    editor.putInt("callCount", count);
                    editor.commit();
                }

            }

            database.close();
            return null;
        }
    }


        }
